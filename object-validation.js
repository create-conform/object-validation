/////////////////////////////////////////////////////////////////////////////////////////////
//
// object-validation
//
//    Library for processing and validating objects.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var validate = require("validate");
var type =     require("type");

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Object class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function ObjectValidator() {
    var self = this;

    this.FUNCTION = "object-property-function";
    this.PROPERTY = "object-property-property";

    this.hasFunction = function(obj) {
        return scan(obj, "[object Function]");
    };
    this.hasProperty = function(obj) {
        for (var p in obj) {
            return true;
        }
        return false;
    };

    // validator
    this.getProperties = function(obj) {
        var props = [];
        if (self.hasFunction(obj)) { props.push(self.FUNCTION); }
        if (self.hasProperty(obj)) { props.push(self.PROPERTY); }
        return props;
    };
    this.isValid = function(obj) {
        return Object.prototype.toString.call(obj) === "[object Object]";
    };

    function scan(obj, type) {
        for (var p in obj) {
            var objPType = Object.prototype.toString.call(obj[p]);
            if (objPType == type ||
                Object.getPrototypeOf(obj[p]) == type) {
                return true;
            }

            // recursion
            //if (objPType == "[object Object]") {
            //    if (scan(obj[p], type)) {
            //        return true;
            //    }
            //}
        }
        return false;
    }
}
// set validator prototype
ObjectValidator.prototype = validate.Validator;

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = new ObjectValidator();